<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(

            [ 
                    [
                     'title' => 'testTask1',
                     'user_id'=> '1',
                    'created_at'=> date('Y-m-d G:i:s'),
                    ] ,
                    [
                    'title' => 'testTask2',
                    'user_id'=> '1',
                    'created_at'=> date('Y-m-d G:i:s'),
                    ] ,
                    [
                    'title' => 'testTask3',
                     'user_id'=> '1',
                    'created_at'=> date('Y-m-d G:i:s'),
                    ] ,
                    
            ]
                    );
    }
}
